﻿using System;
using Microsoft.AspNetCore.Mvc;
using Clockwork.API.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Clockwork.API.Controllers
{
    [Route("api/[controller]")]
    public class CurrentTimeController : Controller
    {
        private ClockworkContext _db;

        public CurrentTimeController(ClockworkContext db)
        {
            _db = db;
        }

        // GET api/currenttime
        [HttpGet]
        public IActionResult Get(string Offset )
        {
            var utcTime = DateTime.UtcNow;
            var serverTime = DateTime.Now;
            var ip = this.HttpContext.Connection.RemoteIpAddress.ToString();

            var returnVal = new CurrentTimeQuery
            {
                UTCTime = utcTime,
                ClientIp = ip,
                Time = serverTime,
                UTCOffset = Offset
            };


                _db.CurrentTimeQueries.Add(returnVal);
                var count = _db.SaveChanges();
                Console.WriteLine("{0} records saved to database", count);

             

            return Ok(returnVal);
        }

        [HttpGet]
        [Route("Log")]
        public async Task<IActionResult> GetLogs()
        {

            var queries = await _db.CurrentTimeQueries.Select(x => 
                new TimeQuery { 
                    ClientIp = x.ClientIp, 
                    UTCOffset = x.UTCOffset, 
                    UTCTime = x.UTCTime 
                }).ToListAsync();

            return Ok(new DataTableWrapper<TimeQuery>(queries));

           
        }


    }
}
