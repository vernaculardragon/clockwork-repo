﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Clockwork.API.Models
{
    public class TimeQuery
    {
       
        public string ClientIp { get; set; }
        public DateTime UTCTime { get; set; }
        public string UTCOffset { get; set; }
    }

    public class DataTableWrapper<T>
    {
        public DataTableWrapper(List<T> data)
        {
            Data = data;
        }
        public List<T> Data { get; set; }
    }
}
